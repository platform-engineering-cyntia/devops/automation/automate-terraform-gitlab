#!/bin/bash

export GITLAB_ACCESS_TOKEN=glpat-RZZTFS6xbC_bLTgMsizz
export USERNAME=marcoscianci
export ID_PROJECT="45635498" 
export TF_STATE="automate.tf"

terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/$ID_PROJECT/terraform/state/$TF_STATE" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$ID_PROJECT/terraform/state/$TF_STATE/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$ID_PROJECT/terraform/state/$TF_STATE/lock" \
    -backend-config="username=$USERNAME" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
