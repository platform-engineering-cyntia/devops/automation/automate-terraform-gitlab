<div align="center">
<h1>Automate Terraform Gitlab</h1>
    <a href="/docs/terraform-modulos.md">Terraform Modulos</a>
    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="/docs/org-repo.md">Organização do Repositorio</a>
    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="/docs/pre-commit.md">Pre Commit</a>
    &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
    <a href="/docs/diagramas.md">Diagramas</a>
</div>
<hr>

## :raising_hand: O que é esse projeto?

O Projeto Automate Terraform Gitlab automatiza a criação de Grupos, Repositorios e Acessos no Gitlab.

## :construction: Pré-requisitos

The programs and softwares that need to install in your host:

- docker
- git

## :walking: Setup

```bash
Create container execute code terraform:
    make build_image

Access container code terraform
    make run_bash

Run script command-line for terrafom:
    ./terraform-setup.sh
```

## :raised_hand: Considerações
```bash
Sensitive data are stored in a safe, for example, passwords and database access information, end others. Don´t commit these data to the repository.
```
## :raised_hands: Contribuidores

```bash
Marcos Cianci <marcos.cianci@gmail.com>
```
## :bookmark: Licensing
```shell
Marcos Cianci <marcos.cianci@gmail.com> ©2023
```
