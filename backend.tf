### Terraform Backend ###
# backend.tf #

terraform {

  required_version = ">= 1.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/45635498/terraform/state/automate.tf"
    lock_address   = "https://gitlab.com/api/v4/projects/45635498/terraform/state/automate.tf/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/45635498/terraform/state/automate.tf/lock"

  }

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "15.9.0"
    }
  }
}
