<div align="center">
<h1>Modulos Terraform</h1>
</div>
<hr>

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_gitlab_group_aderencia"></a> [gitlab\_group\_aderencia](#module\_gitlab\_group\_aderencia) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_agendamento"></a> [gitlab\_group\_agendamento](#module\_gitlab\_group\_agendamento) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_apps"></a> [gitlab\_group\_apps](#module\_gitlab\_group\_apps) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_beneficios"></a> [gitlab\_group\_beneficios](#module\_gitlab\_group\_beneficios) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_canaisdigitais_qa_developers"></a> [gitlab\_group\_canaisdigitais\_qa\_developers](#module\_gitlab\_group\_canaisdigitais\_qa\_developers) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_canaisdigitais_qa_maintainers"></a> [gitlab\_group\_canaisdigitais\_qa\_maintainers](#module\_gitlab\_group\_canaisdigitais\_qa\_maintainers) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_canaisdigitais_users"></a> [gitlab\_group\_canaisdigitais\_users](#module\_gitlab\_group\_canaisdigitais\_users) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_conta"></a> [gitlab\_group\_conta](#module\_gitlab\_group\_conta) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_core"></a> [gitlab\_group\_core](#module\_gitlab\_group\_core) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_core_bff"></a> [gitlab\_group\_core\_bff](#module\_gitlab\_group\_core\_bff) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_core_site"></a> [gitlab\_group\_core\_site](#module\_gitlab\_group\_core\_site) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_cupom"></a> [gitlab\_group\_cupom](#module\_gitlab\_group\_cupom) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_loyalt"></a> [gitlab\_group\_loyalt](#module\_gitlab\_group\_loyalt) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_group_qa"></a> [gitlab\_group\_qa](#module\_gitlab\_group\_qa) | ./modules/gitlab/gitlab_group | n/a |
| <a name="module_gitlab_project_app"></a> [gitlab\_project\_app](#module\_gitlab\_project\_app) | ./modules/gitlab/gitlab_project | n/a |
| <a name="module_gitlab_project_core_bff"></a> [gitlab\_project\_core\_bff](#module\_gitlab\_project\_core\_bff) | ./modules/gitlab/gitlab_project | n/a |
| <a name="module_gitlab_project_core_bff_poc"></a> [gitlab\_project\_core\_bff\_poc](#module\_gitlab\_project\_core\_bff\_poc) | ./modules/gitlab/gitlab_project | n/a |
| <a name="module_gitlab_project_core_site"></a> [gitlab\_project\_core\_site](#module\_gitlab\_project\_core\_site) | ./modules/gitlab/gitlab_project | n/a |
| <a name="module_gitlab_project_qa_back_bff"></a> [gitlab\_project\_qa\_back\_bff](#module\_gitlab\_project\_qa\_back\_bff) | ./modules/gitlab/gitlab_project | n/a |
| <a name="module_gitlab_project_qa_bff_app"></a> [gitlab\_project\_qa\_bff\_app](#module\_gitlab\_project\_qa\_bff\_app) | ./modules/gitlab/gitlab_project | n/a |
| <a name="module_gitlab_project_qa_front_app"></a> [gitlab\_project\_qa\_front\_app](#module\_gitlab\_project\_qa\_front\_app) | ./modules/gitlab/gitlab_project | n/a |
| <a name="module_gitlab_project_qa_front_magento2"></a> [gitlab\_project\_qa\_front\_magento2](#module\_gitlab\_project\_qa\_front\_magento2) | ./modules/gitlab/gitlab_project | n/a |
| <a name="module_gitlabci_file_repo_app"></a> [gitlabci\_file\_repo\_app](#module\_gitlabci\_file\_repo\_app) | ./modules/gitlab/gitlab_repository_file | n/a |
| <a name="module_gitlabci_file_repo_core_bff"></a> [gitlabci\_file\_repo\_core\_bff](#module\_gitlabci\_file\_repo\_core\_bff) | ./modules/gitlab/gitlab_repository_file | n/a |
| <a name="module_gitlabci_file_repo_core_site"></a> [gitlabci\_file\_repo\_core\_site](#module\_gitlabci\_file\_repo\_core\_site) | ./modules/gitlab/gitlab_repository_file | n/a |

<a href="../README.md">  <-- Voltar </a>
